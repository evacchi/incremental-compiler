package io.github.evacchi.javacc;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;
import java.util.HashMap;
import java.util.stream.Collectors;

import io.github.evacchi.javacc.classfiles.ClassFile;

import static io.github.evacchi.javacc.IncrementalCompiler.JAVA_FILE_EXTENSION;

/**
 * For a given class binary name, it returns the file name that contains it
 */
public class InvertedFileMapper {

    private final HashMap<String, String> binaryNameToSourceFile = new HashMap<>();
    private final Path sourcePath;


    public InvertedFileMapper(Path sourcePath) {
        this.sourcePath = sourcePath;
    }


    Collection<Path> getDependant(Collection<Path> modifiedSources, InvertedClassIndex invertedClassIndex) {
        // resolve files from the inverted index
        return modifiedSources.stream()
                .map(Path::toString)
                .map(s -> s.substring(0, s.length() - JAVA_FILE_EXTENSION.length()))
                .flatMap(s -> invertedClassIndex.getDependants(s).stream())
                .map(binaryNameToSourceFile::get)
                .map(Path::of)
                .filter(p -> Files.exists(sourcePath.resolve(p)))
                .collect(Collectors.toList());
    }

    void merge(FileMapper update) {
        for (FileMapping fileMapping : update.asCollection()) {
            for (ClassFile classFile : fileMapping.classFiles()) {
                binaryNameToSourceFile.put(classFile.binaryName(), fileMapping.source().toString());
            }
        }
    }

    void store(ObjectOutputStream outputStream) throws IOException {
        outputStream.writeObject(binaryNameToSourceFile);
    }

    @SuppressWarnings("unchecked")
    void load(ObjectInputStream inputStream) throws IOException, ClassNotFoundException {
        HashMap<String, String> cache = (HashMap<String, String>) inputStream.readObject();
        this.binaryNameToSourceFile.putAll(cache);
    }
}
