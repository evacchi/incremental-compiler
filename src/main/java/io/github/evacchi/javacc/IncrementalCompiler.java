package io.github.evacchi.javacc;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Path;
import java.util.Collection;
import java.util.logging.Logger;

import io.github.evacchi.javacc.io.DiskFileMapper;
import io.github.evacchi.javacc.io.FileScanner;
import io.github.evacchi.javacc.javac.JavaCompiler;

public class IncrementalCompiler {

    private static final Logger LOGGER = Logger.getAnonymousLogger();

    private static final Path LOCAL_CACHE = Path.of(".cache");
    public static final String JAVA_FILE_EXTENSION = ".java";

    private final Path localCachePath;
    private final Path workingDirectory;
    private final Path sourceDirectory;
    private final Path targetDirectory;
    private final FileMapper fileMapper;
    private final InvertedFileMapper invertedFileMapper;
    private long lastTimestamp = 0;
    private InvertedClassIndex invertedClassIndex;

    public IncrementalCompiler(Path workingDirectory, Path sourceDirectory, Path targetDirectory) {
        this.workingDirectory = workingDirectory.toAbsolutePath();
        this.sourceDirectory = workingDirectory.resolve(sourceDirectory).toAbsolutePath();
        this.targetDirectory = workingDirectory.resolve(targetDirectory).toAbsolutePath();
        this.fileMapper = new FileMapper();
        this.invertedFileMapper = new InvertedFileMapper(this.sourceDirectory);
        this.invertedClassIndex = new InvertedClassIndex(this.targetDirectory);
        this.localCachePath = workingDirectory.resolve(LOCAL_CACHE).toAbsolutePath();
    }

    public FileMapper compile() throws IOException {
        var scanner = new FileScanner(sourceDirectory, JAVA_FILE_EXTENSION);
        var modifiedSources = scanner.scanUpdatedSince(lastTimestamp);
        if (modifiedSources.isEmpty()) {
            LOGGER.info("No updated source files. Stop.");
            return new FileMapper();
        }

        Collection<Path> dependant = this.invertedFileMapper.getDependant(modifiedSources, invertedClassIndex);
        modifiedSources.addAll(dependant);

        // remove all associated class files to account for deleted inner/anonymous/nonpublic classes
        FileMapper removed = fileMapper.removeAll(modifiedSources);
        DiskFileMapper diskFileMapper = new DiskFileMapper(sourceDirectory, targetDirectory, fileMapper);
        diskFileMapper.deleteAll(removed);
        // delete class files that no longer correspond to an existing source file
        diskFileMapper.purgeSources();

        // compile only the modified sources
        JavaCompiler javaCompiler = new JavaCompiler(modifiedSources, sourceDirectory, targetDirectory);
        FileMapper compiled = javaCompiler.compile();

        // update the sourcefile -> classfiles cache
        this.fileMapper.merge(compiled);
        // update the classfile -> sourcefile cache
        this.invertedFileMapper.merge(compiled);
        // throw away and rebuild the inverted class index
        this.invertedClassIndex.rebuild(this.fileMapper);

        this.lastTimestamp = System.currentTimeMillis();

        return compiled;
    }

    void store() throws IOException {
        try (var fileOutputStream = new FileOutputStream(localCachePath.toFile());
             var objectOutputStream = new ObjectOutputStream(fileOutputStream)) {
            fileMapper.store(objectOutputStream);
            invertedFileMapper.store(objectOutputStream);
            invertedClassIndex.store(objectOutputStream);
            objectOutputStream.writeLong(lastTimestamp);
        } finally {
            LOGGER.info("Stored cache at " + localCachePath);
        }
    }

    void load() throws IOException {
        try (var fileInputStream = new FileInputStream(localCachePath.toFile());
             var objectInputStream = new ObjectInputStream(fileInputStream)) {
            fileMapper.load(objectInputStream);
            invertedFileMapper.load(objectInputStream);
            invertedClassIndex.load(objectInputStream);
            lastTimestamp = objectInputStream.readLong();
        } catch (ClassNotFoundException e) {
            throw new Error(e);
        } finally {
            LOGGER.info("Loaded cache at " + localCachePath);
        }
    }
}
