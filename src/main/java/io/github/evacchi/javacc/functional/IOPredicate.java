package io.github.evacchi.javacc.functional;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.function.Predicate;

public interface IOPredicate<T> extends Predicate<T> {

    static <I> Predicate<I> of(IOPredicate<I> p) {
        return p;
    }

    @Override
    default boolean test(T t) {
        try {
            return testThrower(t);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    boolean testThrower(T t) throws IOException;
}
