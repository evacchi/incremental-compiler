package io.github.evacchi.javacc.io;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Collection;
import java.util.stream.Collectors;

import io.github.evacchi.javacc.functional.IOPredicate;

/**
 * Scans all files on a given path, filtering by extension
 */
public class FileScanner {

    private final Path searchPath;
    private final String extension;

    public FileScanner(Path searchPath, String extension) {
        this.searchPath = searchPath;
        this.extension = extension;
    }

    public Collection<Path> scanUpdatedSince(long lastTimestamp) {
        try {
            // naive file extension filtering, ok for poc purposes
            return Files.walk(searchPath)
                    .filter(f -> f.toString().endsWith(extension))
                    .filter(IOPredicate.of(p -> isNewer(p, lastTimestamp)))
                    .map(searchPath::relativize)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    public static boolean isNewer(Path p, long lastTimestamp) throws IOException {
        BasicFileAttributes basicFileAttributes = Files.readAttributes(p, BasicFileAttributes.class);
        return basicFileAttributes.isRegularFile() && basicFileAttributes.lastModifiedTime().toMillis() > lastTimestamp;
    }
}
