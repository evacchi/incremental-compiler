package io.github.evacchi.javacc.io;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.logging.Logger;

import io.github.evacchi.javacc.FileMapper;
import io.github.evacchi.javacc.FileMapping;
import io.github.evacchi.javacc.classfiles.ClassFile;

/**
 * wraps a {@link FileMapper} with I/O-related operations
 * (Currently only deletion)
 */
public class DiskFileMapper {

    private static final Logger LOGGER = Logger.getAnonymousLogger();

    private final Path sourceDirectory;
    private final Path targetDirectory;
    private final FileMapper fileMapper;

    public DiskFileMapper(Path sourceDirectory, Path targetDirectory, FileMapper fileMapper) {
        this.sourceDirectory = sourceDirectory;
        this.targetDirectory = targetDirectory;
        this.fileMapper = fileMapper;
    }

    /**
     * Delete class files that no longer correspond to an existing source file.
     *
     * Scans through all the source file in the fileMapper, and if a file
     * does not exist, deletes all the corresponding class files.
     *
     */
    public void purgeSources() throws IOException {
        FileMapper deleted = new FileMapper();
        try {
            for (FileMapping fileMapping : this.fileMapper.asCollection()) {
                if (!Files.exists(sourceDirectory.resolve(fileMapping.source()))) {
                    this.fileMapper.remove(fileMapping);
                    deleted.update(fileMapping);
                    for (ClassFile classFile : fileMapping.classFiles()) {
                        delete(classFile);
                    }
                }
            }
        } finally {
            if (!deleted.isEmpty())
                LOGGER.info("Deleted the following stale class files (source file does not exist):\n" + deleted);
        }
    }

    /**
     * Deletes all the class files in the given batch
     */
    public void deleteAll(FileMapper batch) throws IOException {
        FileMapper deleted = new FileMapper();
        try {
            for (var mapping : batch.asCollection()) {
                for (var classFile : mapping.classFiles()) {
                    delete(classFile);
                    deleted.update(mapping);
                }
            }
        } finally {
            if (!batch.isEmpty())
                LOGGER.info("Deleted the following stale class files:\n" + deleted);
        }
    }

    private void delete(ClassFile classFile) throws IOException {
        File file = targetDirectory.resolve(classFile.path()).toFile();
        if (file.exists()) {
            boolean deleted = file.delete();
            if (!deleted) {
                throw new IOException("Could not delete file " + classFile);
            }
        }
    }
}
