package io.github.evacchi.javacc;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {

    private static final Logger LOGGER = Logger.getAnonymousLogger();
    private static final String DEFAULT_WORKING_PATH = ".";
    private static final String DEFAULT_SOURCE_PATH = "src/main/java";
    private static final String DEFAULT_OUTPUT_PATH = "target/classes2";

    public static void main(String... args) throws IOException {
        System.out.println("javacc version 0.1.0");

        var workingDirectory = Paths.get(DEFAULT_WORKING_PATH).toAbsolutePath();
        var sourceDirectory = Paths.get(DEFAULT_SOURCE_PATH);
        var targetDirectory = Paths.get(DEFAULT_OUTPUT_PATH);

        switch (args.length) {
            case 3:
                targetDirectory = Paths.get(args[2]);
            case 2:
                sourceDirectory = Paths.get(args[1]);
            case 1:
                workingDirectory = Paths.get(args[0]);
                break;
        }

        System.out.printf("Working Directory: %s\n", workingDirectory);
        System.out.printf("Source Directory:  %s\n", sourceDirectory);
        System.out.printf("Target Directory:  %s\n", targetDirectory);

        var incrementalCompiler = new IncrementalCompiler(
                workingDirectory,
                sourceDirectory,
                targetDirectory);
        try {
            incrementalCompiler.load();
        } catch (IOException e) {
            LOGGER.info("Could not load previous state, starting fresh.");
            LOGGER.log(Level.FINE, "An exception was thrown, will fallback to full rebuild.", e);
        }
        incrementalCompiler.compile();
        incrementalCompiler.store();
    }
}
