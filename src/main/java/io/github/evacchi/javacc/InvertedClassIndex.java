package io.github.evacchi.javacc;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Path;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import io.github.evacchi.javacc.classfiles.ClassFile;
import io.github.evacchi.javacc.classfiles.ClassFileAnalyzer;

/**
 * An index from class name to collection of class names.
 *
 * <p>
 *     For a given class binary name, it returns a collection
 *     of all the classes that reference that binary name.
 * </p>
 *
 * <ul>
 *     <li>e.g. class com.example.A contains a reference to com.example.B;
 *     <li>then the index contains the key <code>com/example/B</code>, value <code>[ com/example/A ]</code>
 * </ul>
 */
public class InvertedClassIndex {

    private final HashMap<String, Set<String>> invertedIndex;
    private final Path targetDirectory;

    public InvertedClassIndex(Path targetDirectory) {
        this.targetDirectory = targetDirectory;
        this.invertedIndex = new HashMap<>();
    }

    Collection<String> getDependants(String key) {
        Set<String> r = invertedIndex.get(key);
        if (r == null) return Collections.emptySet();
        else return Collections.unmodifiableCollection(r);
    }

    void rebuild(FileMapper fileMapper) throws IOException {
        this.invertedIndex.clear();

        // extract all classfiles in the given mapper
        List<ClassFile> classFiles = fileMapper.asCollection()
                .stream()
                .flatMap(fm -> fm.classFiles().stream())
                .collect(Collectors.toList());

        // create set for lookup
        Set<String> binaryNames = classFiles.stream()
                .map(ClassFile::binaryName)
                .collect(Collectors.toSet());

        // for each given classfile, extract the referenced classes
        // and construct the inverted index referenced -> class
        for (ClassFile classFile : classFiles) {
            File file = targetDirectory.resolve(classFile.path()).toFile();
            FileInputStream fis = new FileInputStream(file);
            ClassFileAnalyzer classFileAnalyzer = new ClassFileAnalyzer(fis);
            for (String c : classFileAnalyzer.referencedClasses()) {
                if (!c.equals(classFile.binaryName()) && binaryNames.contains(c)) {
                    invertedIndex.computeIfAbsent(c, k -> new HashSet<>()).add(classFile.binaryName());
                }
            }
        }
    }

    void store(ObjectOutputStream outputStream) throws IOException {
        outputStream.writeObject(invertedIndex);
    }

    @SuppressWarnings("unchecked")
    void load(ObjectInputStream inputStream) throws IOException, ClassNotFoundException {
        HashMap<String, Set<String>> cache = (HashMap<String, Set<String>>) inputStream.readObject();
        this.invertedIndex.putAll(cache);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        for (var e : invertedIndex.entrySet()) {
            sb.append(e.getKey()).append('\n');
            for (String c : e.getValue()) {
                sb.append("    ").append(c).append('\n');
            }
        }
        return sb.toString();
    }
}
