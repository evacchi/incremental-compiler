package io.github.evacchi.javacc;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Collectors;

import io.github.evacchi.javacc.javac.CompilationError;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;

/*
 * Compiles the project rooted at `src/test/resources/it/project1`
 */
public class Project1IT extends BaseIT {

    public Project1IT() {
        super(Path.of("src/test/resources/it/project1"));
    }

    @Test
    void updateReferencedClasses() throws Exception {
        var prj = copySourceToTemp("updateReferencedClasses");

        assertFirstCompile(8, prj);

        var path = Path.of("com/example/A.java");
        var fullPath = prj.resolve(sourceDirectory).resolve(path);
        touch(fullPath);

        var cc = new IncrementalCompiler(prj, sourceDirectory, targetDirectory);
        cc.load();
        var compiled = cc.compile();

        assertEquals(2, compiled.asCollection().size());
        assertEquals(2, compiled.classFiles().size());

        var result =
                new DiskClassLoader(prj.resolve(targetDirectory))
                        .loadAndInvoke("com.example.A", "a");
        assertEquals("a string", result);
    }

    @Test
    void updateLeaf() throws IOException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        var prj = copySourceToTemp("updateLeaf");

        assertFirstCompile(8, prj);

        var path = Path.of("com/example/Main.java");
        var fullPath = prj.resolve(sourceDirectory).resolve(path);
        touch(fullPath);

        var cc = new IncrementalCompiler(prj, sourceDirectory, targetDirectory);
        cc.load();
        var compiled = cc.compile();

        assertEquals(1, compiled.asCollection().size());
        assertEquals(1, compiled.classFiles().size());

        var result =
                new DiskClassLoader(prj.resolve(targetDirectory))
                        .loadAndInvoke("com.example.Main", "main");

        assertEquals("a string", result);
    }

    @Test
    void delete() throws IOException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        var prj = copySourceToTemp("delete");

        assertFirstCompile(8, prj);

        var sourcePath = Path.of("com/example/Main.java");
        var classPath = Path.of("com/example/Main.class");
        var sourceFullPath = prj.resolve(sourceDirectory).resolve(sourcePath);
        var classFullPath = prj.resolve(targetDirectory).resolve(sourcePath);
        Files.delete(sourceFullPath);

        var cc = new IncrementalCompiler(prj, sourceDirectory, targetDirectory);
        cc.load();
        var compiled = cc.compile();

        // we assume no fancy classloading here :)
        assertEquals(0, compiled.asCollection().size(), "No changed source files, no need to rebuild -- even if deleted.");

        var path2 = Path.of("com/example/Main2.java");
        var fullPath2 = prj.resolve(sourceDirectory).resolve(path2);
        touch(fullPath2);

        var compiled2 = cc.compile();
        assertEquals(1, compiled2.asCollection().size(), "One file changed");
        assertFalse(Files.exists(classFullPath), "The class file should have been deleted");

        var result =
                new DiskClassLoader(prj.resolve(targetDirectory))
                        .loadAndInvoke("com.example.Main2", "main");

        assertEquals("a string", result);

        assertThrows(ClassNotFoundException.class,
                     () -> new DiskClassLoader(prj.resolve(targetDirectory))
                             .loadAndInvoke("com.example.Main", "main"));
    }

    @Test
    void rename() throws IOException {
        var prj = copySourceToTemp("rename");

        assertFirstCompile(8, prj);

        var originalPath = Path.of("com/example/Main.java");
        var originalClassPath = Path.of("com/example/Main.class");
        var originalFullPath = prj.resolve(sourceDirectory).resolve(originalPath);
        var newPath = Path.of("com/example/NewMain.java");
        var newFullPath = prj.resolve(sourceDirectory).resolve(newPath);
        var originalClassFullPath = prj.resolve(targetDirectory).resolve(originalClassPath);
        var updated = Files.readString(originalFullPath).replace("Main", "NewMain");
        Files.delete(originalFullPath);
        Files.write(newFullPath, updated.getBytes());
        touch(newFullPath);

        var cc = new IncrementalCompiler(prj, sourceDirectory, targetDirectory);
        cc.load();
        var compiled = cc.compile();
        assertEquals(1, compiled.asCollection().size(), "One file changed");
        assertFalse(Files.exists(originalClassFullPath), "The original class file should have been deleted");
    }

    @Test
    void updateInnerAndNonpublicClasses() throws IOException {
        var prj = copySourceToTemp("updateInnerAndNonpublicClasses");

        assertFirstCompile(8, prj);

        var path = Path.of("com/example/Main2.java");
        var fullPath = prj.resolve(sourceDirectory).resolve(path);
        touch(fullPath);

        var cc = new IncrementalCompiler(prj, sourceDirectory, targetDirectory);
        cc.load();
        var compiled = cc.compile();

        assertEquals(1, compiled.asCollection().size(), "Only 1 source file");
        assertEquals(3, compiled.classFiles().size(), "3 generated class files");
    }

    @Test
    void defaultMethodToAbstractShouldFailCompileTime() throws IOException {
        var prj = copySourceToTemp("defaultMethod");

        assertFirstCompile(8, prj);

        var path = Path.of("com/example/I.java");
        var fullPath = prj.resolve(sourceDirectory).resolve(path);

        // turn a default interface method to an abstract method
        // the interface will compile fine, but dependent classes using the method
        // would fail at runtime with a linking error: we ensure we compile dependants
        // thus forcing a compile-time error
        var editedFile = Files.readAllLines(fullPath).stream()
                .filter(s -> !s.contains("/**DELETEME**/"))
                .collect(Collectors.toList());
        Files.write(fullPath, editedFile);
        touch(fullPath);

        var cc = new IncrementalCompiler(prj, sourceDirectory, targetDirectory);
        cc.load();
        assertThrows(CompilationError.class, cc::compile);
    }
}
