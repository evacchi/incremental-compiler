package io.github.evacchi.javacc;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

import io.github.evacchi.javacc.javac.JavaCompiler;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class JavaCompilerIT extends BaseIT {

    public JavaCompilerIT() {
        super(Path.of("src/test/resources/it/project1"));
    }

    @Test
    public void compile() throws IOException {
        var prj = copySourceToTemp("compile");

        var path = Path.of("com/example/A.java");
        var fullPath = prj.resolve(sourceDirectory).resolve(path);

        var c = new JavaCompiler(List.of(fullPath), prj.resolve(sourceDirectory), prj.resolve(targetDirectory));
        var compiled = c.compile();

        assertEquals(1, compiled.asCollection().size());
        assertEquals(1, compiled.classFiles().size());

    }

}
