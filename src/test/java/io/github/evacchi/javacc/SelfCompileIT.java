package io.github.evacchi.javacc;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SelfCompileIT extends BaseIT {

    SelfCompileIT() {
        super(Path.of("."));
    }

    @Test
    void compileTree() throws IOException {
        var prj = copySourceToTemp("compileTree");
        var cc = new IncrementalCompiler(prj, sourceDirectory, targetDirectory);
        var compiled = cc.compile();
        assertEquals(17, compiled.classFiles().size());
    }

    @Test
    void compileUpdatedFile() throws IOException, InterruptedException {
        var prj = copySourceToTemp("compileUpdatedFile");

        assertFirstCompile(17, prj);

        // Main contains only 1 class
        var pMain = prj.resolve(sourceDirectory).resolve("io/github/evacchi/javacc/Main.java");
        touch(pMain);

        var cc = new IncrementalCompiler(prj, sourceDirectory, targetDirectory);
        cc.load();
        var compiled = cc.compile();
        assertEquals(1, compiled.classFiles().size());
    }

    @Test
    void updateReferencedClasses() throws IOException {
        var prj = copySourceToTemp("updateReferencedClasses");

        assertFirstCompile(17, prj);

        var pInvertedClassIndex = Path.of("io/github/evacchi/javacc/InvertedClassIndex.java");
        var fullPath = prj.resolve(sourceDirectory).resolve(pInvertedClassIndex);
        touch(fullPath);

        var cc = new IncrementalCompiler(prj, sourceDirectory, targetDirectory);
        cc.load();
        var compiled = cc.compile();
        assertEquals(3, compiled.asCollection().size());
        assertEquals(3, compiled.classFiles().size());
    }

    @Test
    void compileUpdatedFileWithInnerClasses() throws IOException, InterruptedException {
        var prj = copySourceToTemp("compileUpdatedFileWithInnerClasses");

        assertFirstCompile(17, prj);

        var rpRecordingJavaFileManager = Path.of("io/github/evacchi/javacc/javac/RecordingJavaFileManager.java");
        var pRecordingJavaFileManager =
                prj.resolve(sourceDirectory)
                        .resolve(rpRecordingJavaFileManager);
        touch(pRecordingJavaFileManager);

        var cc = new IncrementalCompiler(prj, sourceDirectory, targetDirectory);
        cc.load();
        var compiled = cc.compile();
        // RecordingJavaFileManager.java contains 3 classes (1 class + 2 anonymous inner classes)
        assertEquals(3, compiled.get(rpRecordingJavaFileManager).size());
        // total number is 3 + 1 affected dependant (JavaCompiler.java)
        assertEquals(4, compiled.classFiles().size());
    }

    @Test
    void updateFilesOnMove() throws IOException, InterruptedException {
        var prj = copySourceToTemp("updateFilesOnMove");

        assertFirstCompile(17, prj);

        // Main contains only 1 class
        var pMain = Path.of("io/github/evacchi/javacc/Main.java");
        var pMainFull = prj.resolve(sourceDirectory).resolve(pMain);
        var newMain = Path.of("io/github/evacchi/javacc/NewMain.java");
        var newMainFull = prj.resolve(sourceDirectory).resolve(newMain);
        var updatedSource = Files.readString(pMainFull).replace("Main", "NewMain");
        Files.write(newMainFull, updatedSource.getBytes(), StandardOpenOption.CREATE_NEW);
        Files.setLastModifiedTime(newMainFull, theFuture());
        Files.delete(pMainFull);

        var cc = new IncrementalCompiler(prj, sourceDirectory, targetDirectory);
        cc.load();
        var compiled = cc.compile();
        assertEquals(compiled.get(newMain).size(), 1);
        assertEquals(compiled.get(pMain).size(), 0);
        assertEquals(1, compiled.classFiles().size());
    }

}
