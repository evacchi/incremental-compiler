javacc: javac cached
====================

A tiny incremental compiler PoC.

## Compiling

    mvn verify

## Usage

we suggest to alias:

    alias javac="java -jar $PWD/target/incremental-compiler-1.0-SNAPSHOT.jar"

usage:

    javacc [ workingDirectory [ sourceDirectory [ targetDirectory ] ] ] 

Arguments are positional and optional.

1. `workingDirectory` (default: '`.`') is the root of the project that you compile
2. `sourceDirectory` (default: 'src/main/java') is the root of the source code 
3. `targetDirectory` (default: `target/classes2`) is the output directory

A hidden `.cache` file is generated in the `workingDirectory`. This file holds the metadata
to support incremental compilation. You can `rm .cache` at any time to start afresh.

### Example

    mvn verify
    java -jar target/incremental-compiler-1.0-SNAPSHOT.jar
    find target/classes2

You can verify that the compiler compiled itself in the `target/classes2` directory.

### Test Cases

    mvn verify

Test cases are mostly integration tests, because the implementation heavily
relies on the file system. The test cases copy source code to a temporary location
and build it from there. Some test cases also load the compiled classes 
into a custom classloader to verify they run successfully.

## Design and Limitations

### Cache

The following indices are created and persisted to disk in the `.cache` file:

- `FileMapper`. Maps a **path** to a **source** file into a **collection** 
  of the corresponding generated **class file paths**  (1-to-many)
- `InvertedFileMapper`. Maps a **path** to a **class file** into 
  the **path** of the **source file** that originated it (1-to-1)
- `InvertedClassIndex`. Maps a **binary class name** 
  (i.e. a "slashed" class name such as `com/example/MyClass`) into a
  **collection** of **binary class names** that depend on that given class
  
  *e.g.*: `com/example/A` maps to `com/example/B` iff `com/example/B` contains a reference to `com/example/A`

The **timestamp** of the last succesfull build is also persisted in the same `.cache` file.


### Compilation Procedure

0. Upon instantiation, we load default values. User may call the `load()` method to load the cache from disk

1. The `sourceDirectory` is scanned for `*.java` files that are newer than the last
observed build timestamp (default is `0`).
2. These files are added to a list of `modifiedSource`
3. If `modifiedSources` is empty, the compilation procedure quits immediately.
4. The `InvertedFileMapper` is queried with the `modifiedSources` to lookup the corresponding class files; 
     let's call these `modifiedClasses`
    - the `InvertedClassIndex` is also queried to return the collection of class files that depend on the `modifiedClasses`
    - the `InvertedFileMapper` maps back the returned binary names into source code file paths
    - the result of the query is added to `modifiedSources`
    
5. The following class files are now deleted:
      
      a. All the corresponding class files of the files now in `modifiedSources` are *deleted*,
   
      b. Missing source code files are checked, and their corresponding class files are *deleted* as well
   
    The reason for *deleting* is that nonpublic or inner classes of these files may have changed. 
    The class files that are still relevant will be re-created anyway because `javac` works on a per-file basis
    (in other words, it doesn't make sense to reason at a finer-grained level)

6. The list of `modifiedSources` is now fed into the `javac` compiler with `targetDirectory` as a class path.
7. During compilation, we hook into the `javac` API to record the list of generated class files, mapping them
   to their originating source code files.
8. If the compilation fails a `CompilationError` is thrown printing a list of collected `javac` diagnostic   
9. If the compilation succeeds, a new `FileMapper` with the collected source/classes mapping is returned. 
10. We now update the cache for storage: 
    
    a. the compilation result is merged the global `FileMapper` 
    
    b. and it is used to update the `InvertedFileMapper`, 
    
    c. the `InvertedClassIndex` is always rebuilt from scratch, scanning the generated class files from disk

    d. we register the current timestamp
    
11. the `compile()` method returns the results; users are able to invoke `store()` to persist the `.cache` file to disk.
 

### Caveats

**WARNING!** The project was only tested on a Linux system, path or file system handling 
may be buggy on other systems.

#### Class Path and Multi-module

ClassPath is resolved only within the project and it does not account for
library dependencies -- this should not be a big issues, and it should be easy to support.
Project is assumed to consist of only 1 module, *multi-module compilation is not supported*.


#### File changes

In order to keep it simple, `javacc` uses a very simplistic approach to checking files 
for change; i.e., *last modified* date of the source code files. 
This causes unchanged files that have only been `touch`ed to be *always* recompiled. 
An improved version may compute a *digest* of the files and check against that instead.

#### Dependency Tracking

In the initial research phase of `javacc`, I considered hooking into `javac` to construct a dependency graph 
across source code files. A finer grained approach would also take into account symbol lookups, 
and references across class files to these symbols. 
However, the following considerations make maintenance of said graph mostly unnecessary
at least for the use cases that were considered.

Consider the following example: 

- class A is a dependency of class B, and B is a dependency of C (`A -> B -> C`)
- when `C` is modified, then only `C` has to be recompiled
- when `B` is modified, then `C` has to be recompiled, but only to make sure that 
  possible linking errors are caught at compile time (i.e. fields or methods have changed their signature);
  otherwise it is often possible to recompile a single Java file, at the cost of getting a runtime link error
  on certain signature changes
- when `A` is modified, then `B` has to be recompiled for the reasons above, but,
  possibly counterintuitively, not `C`, unless `B` has been modified has well. 
  In fact, if `B` has not changed, then `C` should not break (*).
  
Thus, we assume that is not necessary to perform a complete visit of the inverted dependency graph, 
but, it is enough to visit only the outgoing edges of each modified class, and stop the visit there.
Or, to put it differently, if `C` is the collection of classes, 
`E ⊆  C × C` is the collection  of edges, then the collection of classes that must be recompiled 
is the collection of all `c, c'` such as `(c, c') ∈ E` and at least `c` is a modified class.

(*) note, to be more complete: if `A -> B -> C`, with `A` modified it may be possible 
for class `C` to break if there is a chain of inheritance involved: 
for the purposes of this exercise, this was not taken into account, as it would
make indexing more involved.

#### Indexing Cross References

The `InvertedClassIndex` is always rebuilt from scratch, scanning the generated class files from disk.
I considered extracting this piece of information from `javac`: I would consider this the "proper"
way to do it; but scanning the generated class files was much easier, and it works for the purpose
of a simple PoC.

Because it is a "quick and dirty" solution, this is also the only index that is not updated
across compilations, and rather, it is thrown away and recreated each time.

#### Persistence

`javacc` uses plain old Java serialization, which is nor efficient, nor secure, but it is good enough
for the purposes of a PoC. For the same reason, the values are generally store as strings,
and conversions at the user API level are performed (e.g. between strings and paths).

I considered using a more sophisticated indexing mechanism; literature [1,2] shows that 
Datalog can be employed to effect to query symbol information. Paired with a persistable
datalog store, it would render custom maintenance of the cache easier, while providing
a more efficient way to query it. Another possible candidate would be to use 
a simple embedded SQL engine such as SQLite or Apache Derby.

At the end, I have decided to roll my own simple file-based solution, 
because it was quicker, and for the purposes of a PoC it seemed good enough.

## References

Some indicative references

[1] Alpuente, María, et al. 
"DATALOG_SOLVE: A Datalog-Based Demand-Driven Program Analyzer." 
Electronic Notes in Theoretical Computer Science 248 (2009): 57-66.

[2] Smaragdakis, Yannis, and Martin Bravenboer. 
"Using Datalog for fast and easy program analysis." 
International Datalog 2.0 Workshop. Springer, Berlin, Heidelberg, 2010.
